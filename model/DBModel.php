<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
			try {
				$this->db = new PDO('mysql:host=localhost;dbname=test', 'root', '');
				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} catch (PDOException $e) {
				echo 'connection failed: ' . $e->getMessage();
			}
			
            // Create PDO connection
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		/*
		$stm = $this->db->query('SELECT* FROM book ORDER BY id');
		return array_map (function($row) {
		return new book ($row['title'], $row['Author'], $row['description'], $row['id']);
		$stm->fetchAll(PDO::FETCH_ASSOC);
		*/
		try 
		{
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$booklist = array();
			foreach($this->db->query('SELECT * FROM book ') as $row){
				$booklist[] = new Book($row['title'],$row['author'],$row['description'],$row['id']);
			}                                    
        return $booklist;
		
		} catch (PDOException $e) {
			echo 'Function failed: ' . $e-getMessage();
		}
		
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		
		try 
		{
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$stmt = $this->db->prepare("SELECT*FROM book WHERE id=?");
			$stmt->execute(array($id));
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
        
			if (row != " " )
			{
				$book = new Book($row['title'],$row['author'],$row['description'],$row['id']);
			}
			else{
				$book = NULL;
			}
			return $book;
		}catch (PDOException $e) {
			echo 'Function failed: ' . $e-getMessage();
		}

    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
	
    {
		try 
		{
			if ($book->title != '' && $book->author != '')
        {
				if($book->description == '')
					{$book->description = NULL;}


				$stmt = $this->db->prepare(
				"INSERT INTO Book (title, author, description)  VALUES(:title, :author, :description)");
				$stmt->execute(array(':title' => $book->title, ':author' => $book->author, ':description' => $book->description));
				$affected_rows = $stmt->rowCount();
        }
				else {
					$view = new ErrorView();
					$view->create();
				}



		} catch  (PDOException $e) {
			echo 'Function failed: ' . $e-getMessage();
		}

		
		
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
		//$stmt = $this->db->prepare("UPDATE book SET title =: title, SET author= :author, SET description = :description, WHERE id =:id");
		//$stmt->execute(array($book->id, $book->title,$book->author, $book->description));
		try {
						if ($book->title != '' && $book->author != '')
			{
				if ($book->description == '')
					{$book->description = NULL;}
			
			//$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$stmt = $this->db->prepare("UPDATE Book SET title = :title, author = :author, description = :description WHERE id = :id");
			
				$stmt->bindValue("id", $book->id);
				$stmt->bindValue("title", $book->title);
				$stmt->bindValue("author", $book->author);
				$stmt->bindValue("description", $book->description);
				$stmt->execute();
			}
			else
			{
			$view = new ErrorView();
			$view->create();
			}
		} catch (PDOException $e) {
			echo 'Function failed: ' . $e-getMessage();
		}

		
			
		

		
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
		try {
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$stmt = $this->db->prepare("DELETE FROM book WHERE id=:id");
			$stmt->bindValue(':id', $id, PDO::PARAM_STR);
			$stmt->execute();
		} catch (PDOException $e) {
			echo 'Function failed: ' . $e-getMessage();
		}

		
    }
	
}

?>